package calc;

public class Res {
        String val1;
        String val2;
        String oper;

    public String getVal1() {
        return val1;
    }

    public String getVal2() {
        return val2;
    }

    public String getOper() {
        return oper;
    }

    @Override
    public String toString() {
        return "Res{" +
                "val1='" + val1 + '\'' +
                ", val2='" + val2 + '\'' +
                ", oper='" + oper + '\'' +
                '}';
    }
}
