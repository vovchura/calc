package calc;

public class Calc {
    public static void main(String[] args) {
        Calc calc = new Calc();
        calc.calc("12*4");
    }

    private void calc(String string) {


        if (string.contains("-")) {
            String[] parts = string.split("[-]");
            String part1 = parts[0];
            String part2 = parts[1];
            int i1 = Integer.parseInt(part1);
            int i2 = Integer.parseInt(part2);
            System.out.println(i1 - i2);
        }
        if (string.contains("/")) {
            String[] parts = string.split("[/]");
            String part1 = parts[0];
            String part2 = parts[1];
            int i1 = Integer.parseInt(part1);
            int i2 = Integer.parseInt(part2);
            System.out.println(i1 / i2);
        }
        if (string.contains("+")) {
            String[] parts = string.split("[+]");
            String part1 = parts[0];
            String part2 = parts[1];
            int i1 = Integer.parseInt(part1);
            int i2 = Integer.parseInt(part2);
            System.out.println(i1 + i2);
        }
        if (string.contains("*")) {
            String[] parts = string.split("[*]");
            String part1 = parts[0];
            String part2 = parts[1];
            int i1 = Integer.parseInt(part1);
            int i2 = Integer.parseInt(part2);
            System.out.println(i1 * i2);
        }
    }
}